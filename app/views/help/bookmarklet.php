<div class="help">
  <h1>Help: Bookmarklet</h1>

  <div class="section">
    <p>Bookmark the following link: <a href="javascript:location.href='http://<?= CONFIG()->server_host ?>/post/upload?url='+encodeURIComponent(location.href)">Post to <?= CONFIG()->app_name ?></a>.</p>
  
    <div class="section">
      <h4>How to Use</h4>
      <ul>
        <li>Click on the bookmarklet.</li>
        <li>You'll be redirected to the upload page where you can fill out the tags and set the rating.</li>
      </ul>
    </div>
    <div class="section">
        <h4>Other bookmarklets</h4>
        <ul>
            <li><a href="javascript:location.href='http://<?= CONFIG()->server_host ?>/post/upload?url='+encodeURIComponent(location.href)+'&rating=s'">Post to <?= CONFIG()->app_name ?> (safe)</a></li>
            <li><a href="javascript:location.href='http://<?= CONFIG()->server_host ?>/post/upload?url='+encodeURIComponent(location.href)+'&rating=q'">Post to <?= CONFIG()->app_name ?> (questionable)</a></li>
            <li><a href="javascript:location.href='http://<?= CONFIG()->server_host ?>/post/upload?url='+encodeURIComponent(location.href)+'&rating=e'">Post to <?= CONFIG()->app_name ?> (explicit)</a></li>
        </ul>
        <p>You can use these as alternatives to specify the rating while uploading a file.</p>
    </div>
  </div>
</div>

<?php $this->contentFor("subnavbar", function() { ?>
  <li><?= $this->linkTo("Help", "#index") ?></li>
<?php }) ?>